package com.example.examen_mathapp

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_substration.*
import java.lang.Math.abs
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SubstrationFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SubstrationFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class SubstrationFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private val random = Random()
    private var resultadoReal: Int= 0
    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private  lateinit var  countDownTimer: CountDownTimer
    private var score:Int = 0
    var timeLeft = 10
    var numRounds = 1
    internal lateinit var vista: TextView
    internal lateinit var userInput: TextView
    internal lateinit var sendB: Button

    internal  lateinit var roundView:TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_substration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.num1Txt.text = (random.nextInt(20-1)+1).toString()
        this.num2Txt.text = (random.nextInt(20-1)+1).toString()
        this.resultadoReal = abs(this.num1Txt.text.toString().toInt() - this.num2Txt.text.toString().toInt())
        this.vista = view.findViewById(R.id.timeLeftM)
        this.userInput = view.findViewById(R.id.userInput)
        this.sendB = view.findViewById(R.id.goBtn)
        this.roundView = view.findViewById(R.id.roundTxtD)
        startGame()

    }
    private fun startGame() {
        this.timeLeft = 10
        this.score = 0
        this.roundView.text = this.numRounds.toString()
        this.numRounds+=1
        countDownTimer = object: CountDownTimer(this.initialCountDown,countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                //To change body of created functions use File | Settings | File Templates.
                timeLeft = millisUntilFinished.toInt()/1000
                vista.text = timeLeft.toString()
                sendB.setOnClickListener {
                    verificarScore()


                }

            }



            override fun onFinish() {
                if (numRounds<=5){
                    resetGame()
                }
            }



        }
        countDownTimer.start()
    }



    fun verificarScore(){
        var userInput = userInput.text.toString().toInt()
        if(timeLeft>8 && timeLeft<10 && userInput==resultadoReal){
            score = 100
            Toast.makeText(activity, "your score: "+score.toString(),
                Toast.LENGTH_LONG).show()
        }else if(timeLeft>5 && timeLeft<=8 && userInput==resultadoReal){
            score = 50
            Toast.makeText(activity, "your score: "+score.toString(),
                Toast.LENGTH_LONG).show()
        }else if(timeLeft>0 && timeLeft<=5 && userInput==resultadoReal) {
            score = 10
            Toast.makeText(activity, "your score: "+score.toString(),
                Toast.LENGTH_LONG).show()

        }else if(userInput!=resultadoReal){
            score = 0
            Toast.makeText(activity, "the input answer is wrong your score: "+score.toString(),
                Toast.LENGTH_LONG).show()
        }

    }

    fun resetGame() {

        this.timeLeft = 10
        this.score = 0
        this.roundView.text = this.numRounds.toString()
        this.numRounds+=1
        countDownTimer = object: CountDownTimer(this.initialCountDown,countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                //To change body of created functions use File | Settings | File Templates.
                timeLeft = millisUntilFinished.toInt()/1000
                vista.text = timeLeft.toString()
                sendB.setOnClickListener {
                    verificarScore()


                }

            }



            override fun onFinish() {
                if (numRounds<=5){
                    startGame()
                }
            }



        }
        countDownTimer.start()
    }
    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SubstrationFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SubstrationFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
